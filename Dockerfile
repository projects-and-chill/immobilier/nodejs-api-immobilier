FROM node:16.14-alpine3.15 AS alpine
ENV NODE_ENV=production
RUN apk add --no-cache curl dumb-init
WORKDIR /usr/src/app
COPY ./ ./
RUN npm i
CMD ["dumb-init", "node", "app.js"]
